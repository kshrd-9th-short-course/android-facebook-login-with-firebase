package com.chanchhaya.note.firebaseapp;

import android.app.Application;

import com.google.firebase.FirebaseApp;

public class ApplicationConfig extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
    }

}
